﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using ExcentOne.Core.Data.Extensions;
using Serilog;

namespace ExcentOne.Core.Data.Sql
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public static class SqlHelper
    {
        #region "Constants"

        public const int TimeoutDefault = 3600;

        #endregion

        #region "Stored Procedures"

        public static DataTable ExecuteStoredProcedureWithDataTable(string connectionString, string ProcedureName,
            List<SqlParameter> parameters, int TimeOut = TimeoutDefault)
        {
            DataTable dt = null;

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = ProcedureName;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = TimeOut;
                    if (parameters != null)
                        foreach (var p in parameters)
                            command.Parameters.Add(p);

                    var da = new SqlDataAdapter(command);
                    var ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables.Count > 0) dt = ds.Tables[0];
                }

                conn.Close();
            }

            return dt;
        }

        [SuppressMessage("ReSharper", "RedundantAssignment")]
        public static int ExecuteStoredProcedureWithNoReturn(string connectionString, string ProcedureName,
            List<SqlParameter> parameters, int TimeOut = TimeoutDefault)
        {
            var rows = -1;
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = ProcedureName;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = TimeOut;
                    if (parameters != null)
                        foreach (var p in parameters)
                            command.Parameters.Add(p);
                    rows = command.ExecuteNonQuery();
                }

                conn.Close();
            }

            return rows;
        }

        public static T ExecuteStoredProcedureWithParametersToScalar<T>(string connectionString, string ProcedureName,
            List<SqlParameter> parameters, int TimeOut = TimeoutDefault)
        {
            T data;
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = ProcedureName;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = TimeOut;
                    if (parameters != null)
                        foreach (var p in parameters)
                            command.Parameters.Add(p);
                    data = (T) command.ExecuteScalar();
                }

                conn.Close();
            }

            return data;
        }

        #endregion

        #region "SQL w. Parameters"

        public static void ExecuteBulkSql(string connectionString, DataTable data, string tableName,
            bool isEmptyTable = false, int batchSize = 5000, int TimeOut = TimeoutDefault)
        {
            if (!data.HasRows()) return;

            using (var conDB = new SqlConnection(connectionString))
            {
                conDB.Open();

                if (isEmptyTable)
                    //delete existing records
                    using (var command = new SqlCommand($"DELETE FROM {tableName}", conDB))
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandTimeout = TimeOut;
                        command.ExecuteNonQuery();
                    }

                //bulk insert
                using (var bulk = new SqlBulkCopy(conDB))
                {
                    bulk.SqlRowsCopied += OnSqlRowsTransfer;
                    bulk.NotifyAfter = batchSize;
                    bulk.BatchSize = batchSize;
                    bulk.DestinationTableName = tableName;
                    bulk.WriteToServer(data);
                }
            }
        }

        public static void ExecuteLikeForLikeBulkSql(string sourceConnectionString, string destinationConnectionString,
            string tableName, string sourceTableFilter,
            bool isEmptyTable = false, int batchSize = 5000, int TimeOut = TimeoutDefault)
        {
            ExecuteBulkSql(sourceConnectionString, destinationConnectionString, tableName, sourceTableFilter,
                tableName, isEmptyTable, batchSize, TimeOut);
        }

        public static void ExecuteBulkSql(string sourceConnectionString, string destinationConnectionString,
            string sourceTableName, string sourceTableFilter, string destinationTableName,
            bool isEmptyTable = false, int batchSize = 5000, int TimeOut = TimeoutDefault)
        {
            using (var sourceDb = new SqlConnection(sourceConnectionString))
            {
                using (var destinationDb = new SqlConnection(destinationConnectionString))
                {
                    sourceDb.Open();
                    destinationDb.Open();

                    if (isEmptyTable)
                        //delete existing records
                        using (var command = new SqlCommand($"DELETE FROM {destinationTableName}", destinationDb))
                        {
                            command.CommandType = CommandType.Text;
                            command.CommandTimeout = TimeOut;
                            command.ExecuteNonQuery();
                        }

                    var sourceCommand = new SqlCommand($"SELECT * FROM {sourceTableName} {sourceTableFilter}", sourceDb);
                    var sourceReader = sourceCommand.ExecuteReader();

                    //bulk insert
                    using (var bulk = new SqlBulkCopy(destinationDb))
                    {
                        bulk.SqlRowsCopied += OnSqlRowsTransfer;
                        bulk.NotifyAfter = batchSize;
                        bulk.BatchSize = batchSize;
                        bulk.DestinationTableName = destinationTableName;
                        bulk.WriteToServer(sourceReader);
                    }
                }
            }
        }

        private static void OnSqlRowsTransfer(object sender, SqlRowsCopiedEventArgs e)
        {
            Log.Debug("Copied {0} so far...", e.RowsCopied);
        }

        public static int ExecuteSqlWithParametersNoReturn(string connectionString, string SQL,
            CommandType commandType, List<SqlParameter> parameters, int TimeOut = TimeoutDefault)
        {
            const int iRows = 0;

            using (var conDB = new SqlConnection(connectionString))
            {
                conDB.Open();
                using (var command = new SqlCommand(SQL, conDB))
                {
                    command.CommandType = commandType;
                    command.CommandTimeout = TimeOut;
                    if (parameters != null)
                        foreach (var p in parameters)
                            command.Parameters.Add(p);
                    command.ExecuteNonQuery();
                }
            }

            return iRows;
        }

        public static DataTable ExecuteSqlWithParametersToDataTable(string connectionString, string SQL,
            CommandType commandType, List<SqlParameter> parameters = null, int TimeOut = TimeoutDefault)
        {
            DataTable dt;

            using (var conDB = new SqlConnection(connectionString))
            {
                conDB.Open();
                using (var command = new SqlCommand(SQL, conDB))
                {
                    command.CommandType = commandType;
                    command.CommandTimeout = TimeOut;
                    if (parameters != null)
                        foreach (var p in parameters)
                            command.Parameters.Add(p);
                    var rd = command.ExecuteReader();
                    dt = new DataTable();
                    dt.Load(rd);
                }
            }

            return dt;
        }

        public static T ExecuteSqlWithParametersToScalar<T>(string connectionString, string SQL,
            CommandType commandType, List<SqlParameter> parameters, int TimeOut = TimeoutDefault)
        {
            T data;

            using (var conDB = new SqlConnection(connectionString))
            {
                conDB.Open();
                using (var command = new SqlCommand(SQL, conDB))
                {
                    command.CommandType = commandType;
                    command.CommandTimeout = TimeOut;
                    if (parameters != null)
                        foreach (var p in parameters)
                            command.Parameters.Add(p);
                    var rd = command.ExecuteScalar();
                    data = (T) rd;
                }
            }

            return data;
        }

        #endregion

        #region "Utilities"

        public static bool HasRows(DataTable dt)
        {
            var isOK = dt?.Rows != null && dt.Rows.Count > 0;
            return isOK;
        }

        public static bool HasTables(DataSet ds)
        {
            var isOK = ds?.Tables != null && ds.Tables.Count > 0;
            return isOK;
        }

        public static string FixSqlInText(string inText, string defaultValue, char textDelimiter)
        {
            const char listDelimiter = ',';

            var workText = inText;
            if (string.IsNullOrWhiteSpace(workText)) workText = defaultValue;
            if (string.IsNullOrWhiteSpace(workText)) workText = string.Empty;

            if (string.IsNullOrWhiteSpace(workText)) return workText;

            var buf = workText.Split(new[] {listDelimiter}, StringSplitOptions.None);
            var sb = new StringBuilder();
            foreach (var s in buf)
            {
                if (string.IsNullOrWhiteSpace(s)) continue;
                sb.Append(textDelimiter);
                sb.Append(s.Trim());
                sb.Append(textDelimiter);
                sb.Append(listDelimiter);
            }

            workText = sb.ToString();
            if (string.IsNullOrEmpty(workText) || workText.Length <= 1) return workText;
            workText = workText.Trim();
            workText = workText.Substring(0, workText.Length - 1); // Take off last listDelimiter
            return workText;
        }

        [SuppressMessage("ReSharper", "SwitchStatementMissingSomeCases")]
        public static List<SqlParameter> CleanParameters(List<SqlParameter> parameters)
        {
            foreach (var p in parameters)
            {
                if (p.Value == DBNull.Value) continue;
                switch (p.SqlDbType)
                {
                    case SqlDbType.NText:
                    case SqlDbType.NVarChar:
                    case SqlDbType.Text:
                    case SqlDbType.VarChar:
                        p.Value = SqlTextClean(p.Value.ToString());
                        break;
                }
            }

            return parameters;
        }


        public static string SqlTextClean(string inText)
        {
            return inText.Replace("'", "''");
        }

        public static SqlParameter ParameterBuilder(string name, SqlDbType dbType, object value)
        {
            var p = new SqlParameter(name, dbType) {Value = value};
            return p;
        }

        #endregion
    }
}